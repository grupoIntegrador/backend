package com.iftm.integrador.bia.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "modulo")
public class Modulo implements Serializable {

    private static final long serialVersionUID = 4167905966948571170L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_modulo")
    @SequenceGenerator(name = "seq_modulo", sequenceName = "sequence_modulo", allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @ManyToOne
    @JoinColumn(name="id_sistema", nullable=false)
    private Sistema sistema;

}
