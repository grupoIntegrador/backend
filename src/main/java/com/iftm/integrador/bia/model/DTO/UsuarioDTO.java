package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1094060729223811112L;

    private Integer id;

    private String usuario;

}
