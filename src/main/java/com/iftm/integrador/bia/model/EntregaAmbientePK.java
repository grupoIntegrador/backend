package com.iftm.integrador.bia.model;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class EntregaAmbientePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private  Long atividadeId ;

    private Long ambienteId;

    public EntregaAmbientePK(){}

    public Long getAtividade() {
        return atividadeId;
    }

    public void setAtividade(Long  atividadeId) {
        this.atividadeId = atividadeId;
    }

    public Long getAmbiente() {
        return ambienteId;
    }

    public void setAmbiente(Long ambienteId) {
        this.ambienteId =  ambienteId;
    }
}
