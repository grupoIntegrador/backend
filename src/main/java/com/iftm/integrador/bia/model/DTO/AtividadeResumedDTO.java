package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public interface AtividadeResumedDTO  {

     Long getId();

     String getDescricao();

     String getNomeAtividade();

     Boolean getScripts();

     String getFontes();

     String getModulo();

     String getStatusAtividade();

     String getSistema();

     String getAmbientes();

     String getDataEntrega();

}
