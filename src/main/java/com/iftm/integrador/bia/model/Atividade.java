package com.iftm.integrador.bia.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name="atividade")
public class Atividade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_atividade")
    @SequenceGenerator(name="seq_atividade", sequenceName="sequence_atividade",allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "nome_atividade")
    private String nomeAtividade;

    @OneToOne
    @JoinColumn(name="id_modulo", nullable=false)
    private Modulo modulo;

    @OneToOne
    @JoinColumn(name="id_lider_tecnico", nullable=false)
    private Usuario liderTecnico;

    @OneToOne
    @JoinColumn(name="id_lider_projeto", nullable=false)
    private Usuario liderProjeto;

    @Column(name = "branch")
    private String branch;

    @Column(name = "fontes")
    private String fontes;

    @Column(name = "scripts")
    private Boolean scripts;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "status_atividade")
    private String statusAtividade;

    @Column(name = "release")
    private Date release;

    @Column(name = "in_producao")
    private Boolean inProducao;


}
