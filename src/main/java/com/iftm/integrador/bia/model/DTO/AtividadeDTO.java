package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class AtividadeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String descricao;
    private String nomeAtividade;
    private ModuloDTO moduloDTO;
    private UsuarioDTO liderTecnico;
    private UsuarioDTO liderProjeto;
    private String branch;
    private String fontes;
    private Boolean scripts;
    private String observacao;
    private String statusAtividade;

}
