package com.iftm.integrador.bia.model.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Data
public class SistemaDTO implements Serializable {

    private static final long serialVersionUID = -6077860037483010255L;

    private Integer id;

    private String descricao;

    private Set<ModuloDTO> moduloDTOS;

}
