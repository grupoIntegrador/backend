package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
public class ModuloDTO implements Serializable {

    private static final long serialVersionUID = 4167905966948571170L;

    private Integer id;
    private String descricao;
    private SistemaDTO sistemaDTO;

}
