package com.iftm.integrador.bia.model.DTO;

import javax.persistence.Embeddable;
import java.io.Serializable;

public class EntregaAmbientePKDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private  String atividadeId ;

    private Integer ambienteId;

    public EntregaAmbientePKDTO(){}

    public String getAtividade() {
        return atividadeId;
    }

    public void setAtividade(String  atividadeId) {
        this.atividadeId = atividadeId;
    }

    public Integer getAmbiente() {
        return ambienteId;
    }

    public void setAmbiente(Integer ambienteId) {
        this.ambienteId =  ambienteId;
    }
}
