package com.iftm.integrador.bia.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;


@Data
@Entity
@Table(name="sistema")
public class Sistema implements Serializable {

    private static final long serialVersionUID = -6077860037483010255L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_sistema")
    @SequenceGenerator(name="seq_sistema", sequenceName="sequence_sistema",allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @OneToMany( mappedBy = "sistema")
    @JsonIgnore
    private Set<Modulo> modulos;

}
