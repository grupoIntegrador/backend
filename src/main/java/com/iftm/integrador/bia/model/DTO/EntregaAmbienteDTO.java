package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
public class EntregaAmbienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private EntregaAmbientePKDTO entregaAmbienteId;
    private AtividadeDTO id_atividadeDTO;
    private AmbienteDTO id_ambienteDTO;
    private Date dataEntrega;

}
