package com.iftm.integrador.bia.model.DTO;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class AmbienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String descricao;

}
