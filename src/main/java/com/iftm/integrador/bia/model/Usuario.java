package com.iftm.integrador.bia.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name="usuario")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1094060729223811112L;

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq_usuario")
    @SequenceGenerator(name="seq_usuario", sequenceName="sequence_usuario",allocationSize = 1, initialValue = 1)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "usuario", nullable = false)
    private String usuario;

}
