package com.iftm.integrador.bia.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "entrega_ambiente")
public class EntregaAmbiente implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private EntregaAmbientePK entregaAmbienteId;

    @MapsId("atividadeId")
    @ManyToOne
    @JoinColumn(name = "id_atividade", referencedColumnName = "id", nullable = false)
    private Atividade atividade;


    @MapsId("ambienteId")
    @ManyToOne
    @JoinColumn(name = "id_ambiente", referencedColumnName = "id", nullable = false)
    private Ambiente ambiente;

    @Column(name = "data_entrega", nullable = false)
    private Date dataEntrega;

    @Column(name = "inambiente", nullable = false)
    private Boolean inAmbiente;

}
