package com.iftm.integrador.bia.controller;

import com.iftm.integrador.bia.exception.NotFoundException;
import com.iftm.integrador.bia.model.Atividade;
import com.iftm.integrador.bia.model.DTO.AtividadeDTO;
import com.iftm.integrador.bia.model.DTO.AtividadeResumedDTO;
import com.iftm.integrador.bia.services.atividade.AtividadeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/atividade")
public class AtividadeController {

    @Autowired
    private Environment env;

    @Autowired
    private AtividadeService service;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity findAll(){


        List<Atividade> list = service.getAll();

        List<AtividadeDTO> listAtividadeDTO = convertToDto(list);

        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable("id") Long id) throws NotFoundException {
        Optional<Atividade> ret = service.findById(id);

        Atividade atividade = ret.orElseThrow(() -> new NotFoundException(id));

        AtividadeDTO atividadeDTO = convertToDto(atividade);

        return ResponseEntity.ok().body(ret);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody Atividade dto){
        dto = service.save(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(dto);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody Atividade dto){
        dto = service.save(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.noContent().build();
    }

    @GetMapping("findallresumed")
    public ResponseEntity findAllResumed(){
        List<AtividadeResumedDTO> list = service.findAllResumed();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping("entregues")
    public ResponseEntity findAllEntregues(){
        List<AtividadeResumedDTO> list = service.findAllEntregues();
        return ResponseEntity.ok().body(list);
    }

    @PutMapping(value = "/enviar-producao/{id}")
    public ResponseEntity enviarProducao(@PathVariable("id") Long id){
        service.enviarProducao(id);
        return ResponseEntity.noContent().build();
    }

    private AtividadeDTO convertToDto(Atividade obj) {
        AtividadeDTO dto = modelMapper.map(obj, AtividadeDTO.class);

        return dto;
    }

    private List<AtividadeDTO> convertToDto(List<Atividade> obj) {

        List<AtividadeDTO> dto = obj.stream().map(this::convertToDto)
                .collect(Collectors.toList());

        return dto;
    }
}
