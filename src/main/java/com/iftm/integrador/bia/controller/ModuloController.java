package com.iftm.integrador.bia.controller;

import com.iftm.integrador.bia.model.Modulo;
import com.iftm.integrador.bia.services.modulo.ModuloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/modulo")
public class ModuloController {

    @Autowired
    private ModuloService service;

    @GetMapping
    public ResponseEntity findAll(){
        List<Modulo> list = service.getAll();
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody Modulo dto){
        dto = service.save(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(dto);
    }
}
