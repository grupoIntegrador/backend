package com.iftm.integrador.bia.controller;

import com.iftm.integrador.bia.model.EntregaAmbiente;
import com.iftm.integrador.bia.services.entregaambiente.EntregaAmbienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/entrega-ambiente")
public class EntregaAmbienteController {

    @Autowired
    private EntregaAmbienteService service;

    @GetMapping
    public ResponseEntity findAll(){
        List<EntregaAmbiente> list = service.getAll();
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody EntregaAmbiente dto){
        dto = service.save(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getEntregaAmbienteId()).toUri();

        return ResponseEntity.created(uri).body(dto);
    }


    @Transactional
    @Modifying
    @PutMapping
    public ResponseEntity retirarAmbiente(@RequestBody EntregaAmbiente dto){
         service.retirarAmbiente(dto.getAtividade().getId(),dto.getAmbiente().getId());
       return ResponseEntity.noContent().build();
    }
}
