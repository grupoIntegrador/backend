package com.iftm.integrador.bia.controller;

import com.iftm.integrador.bia.exception.NotFoundException;
import com.iftm.integrador.bia.model.Ambiente;
import com.iftm.integrador.bia.model.Atividade;
import com.iftm.integrador.bia.model.DTO.AtividadeDTO;
import com.iftm.integrador.bia.services.ambiente.AmbienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/ambiente")
public class AmbienteController {

    @Autowired
    private AmbienteService service;

    @GetMapping
    public ResponseEntity findAll(){
        List<Ambiente> list = service.getAll();
        return ResponseEntity.ok().body(list);
    }

    @PostMapping
    public ResponseEntity insert(@RequestBody Ambiente dto){
        dto = service.save(dto);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(dto.getId()).toUri();

        return ResponseEntity.created(uri).body(dto);
    }

    @GetMapping("/find-atividades-entregues/{idAtividade}")
    public ResponseEntity findAmbientesEntreguesByAtividade(@PathVariable("idAtividade") Long idAtividade) throws NotFoundException {
        Optional<List<Ambiente>> ret = service.findAmbientesEntreguesByAtividade(idAtividade);

        List<Ambiente> ambientes = ret.orElseThrow(() -> new NotFoundException(idAtividade));

        return ResponseEntity.ok().body(ambientes);
    }


}
