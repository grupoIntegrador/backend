package com.iftm.integrador.bia.services.usuario;

import com.iftm.integrador.bia.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
