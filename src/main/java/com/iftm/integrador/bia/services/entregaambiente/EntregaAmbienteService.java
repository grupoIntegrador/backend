package com.iftm.integrador.bia.services.entregaambiente;

import com.iftm.integrador.bia.model.EntregaAmbiente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EntregaAmbienteService {
    @Autowired
    private EntregaAmbienteRepository repository;

    public List<EntregaAmbiente> getAll(){
        return repository.findAll();
    }

    public EntregaAmbiente save(EntregaAmbiente obj) {
        return repository.save(obj);
    }

    @Transactional
    @Modifying(clearAutomatically = true)
    public void retirarAmbiente(Long idAtividade, Long idAmbiente) {
         repository.retirarAmbiente(idAtividade,idAmbiente);
    }
}
