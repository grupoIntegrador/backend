package com.iftm.integrador.bia.services.atividade;

import com.iftm.integrador.bia.model.Atividade;
import com.iftm.integrador.bia.model.DTO.AtividadeResumedDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AtividadeRepository extends JpaRepository<Atividade, Long> {

    @Query(value = "select\n" +
            "\ta.id,\n" +
            "\ta.nome_atividade as nomeAtividade,\n" +
            "\ta.descricao,\n" +
            "\ta.scripts,\n" +
            "\ta.fontes,\n" +
            "\ta.status_atividade as statusAtividade,\n" +
            "\ts.descricao as sistema,\n" +
            "\tstring_agg(amb.descricao, ', ') AS ambientes,\n" +
            "\tm.descricao as modulo\n" +
            "\t\t\n" +
            "from atividade a\n" +
            "inner join modulo m \n" +
            "\ton m.id = a.id_modulo\n" +
            "inner join sistema s\n" +
            "\ton s.id = m.id_sistema\n" +
            "left join entrega_ambiente eamb\n" +
            "\ton eamb.id_atividade = a.id and eamb.inambiente = true\n" +
            "left join ambiente amb\n" +
            "\ton amb.id = eamb.id_ambiente\n" +
            "group by a.id, s.descricao, m.descricao", nativeQuery = true)
    List<AtividadeResumedDTO> findAllResumed();

    @Query(value = "select\n" +
            "\ta.id,\n" +
            "\ta.nome_atividade as nomeAtividade,\n" +
            "\ta.descricao,\n" +
            "\ta.scripts,\n" +
            "\ta.fontes,\n" +
            "\tstring_agg(amb.descricao, ', ') AS ambientes,\n" +
            "\tstring_agg(\tto_char(eamb.data_entrega, 'DD/MM/YYYY') , ' - ') AS dataEntrega\n" +
            "\n" +
            "\t\t\n" +
            "from atividade a\n" +
            "inner join modulo m \n" +
            "\ton m.id = a.id_modulo\n" +
            "inner join sistema s\n" +
            "\ton s.id = m.id_sistema\n" +
            "inner join entrega_ambiente eamb\n" +
            "\ton eamb.id_atividade = a.id and eamb.inambiente = true\n" +
            "left join ambiente amb\n" +
            "\ton amb.id = eamb.id_ambiente" +
            "\n" +
            " where eamb.inAmbiente = true group by a.id, s.descricao, m.descricao", nativeQuery = true)
    List<AtividadeResumedDTO> findAllEntregues();

    @Modifying
    @Transactional
    @Query(value = "update atividade set in_producao = true where id = :idAtividade",nativeQuery = true)
    void enviarProducao(@Param("idAtividade") Long id);

}
