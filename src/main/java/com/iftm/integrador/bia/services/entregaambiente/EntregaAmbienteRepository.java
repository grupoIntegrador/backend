package com.iftm.integrador.bia.services.entregaambiente;

import com.iftm.integrador.bia.model.Ambiente;
import com.iftm.integrador.bia.model.Atividade;
import com.iftm.integrador.bia.model.EntregaAmbiente;
import com.iftm.integrador.bia.model.EntregaAmbientePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface EntregaAmbienteRepository extends JpaRepository<EntregaAmbiente, EntregaAmbientePK> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value="update entrega_ambiente set inAmbiente = false where  id_atividade = :idAtividade and id_ambiente= :idAmbiente",nativeQuery = true)
    void retirarAmbiente(@Param("idAtividade") Long idAtividade, @Param("idAmbiente") Long idAmbiente);

}
