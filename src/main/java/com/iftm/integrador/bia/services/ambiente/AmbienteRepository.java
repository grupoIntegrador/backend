package com.iftm.integrador.bia.services.ambiente;

import com.iftm.integrador.bia.model.Ambiente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AmbienteRepository extends JpaRepository<Ambiente, Long> {

    @Query(value="select amb.* \n" +
            "from entrega_ambiente eamb\n" +
            "inner join ambiente amb\n" +
            "on eamb.id_ambiente = amb.id\n" +
            "where eamb.id_atividade = :idAtividade and eamb.inAmbiente = true",nativeQuery = true)
    Optional<List<Ambiente>> findAmbientesEntreguesByAtividade(@Param("idAtividade") Long idAtividade);

}
