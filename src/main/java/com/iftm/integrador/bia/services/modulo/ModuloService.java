package com.iftm.integrador.bia.services.modulo;

import com.iftm.integrador.bia.model.Modulo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuloService {
    @Autowired
    private ModuloRepository repository;

    public List<Modulo> getAll(){
        return repository.findAll();
    }

    public Modulo save(Modulo obj) {
        return repository.save(obj);
    }
}
