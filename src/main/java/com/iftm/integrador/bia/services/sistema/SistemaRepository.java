package com.iftm.integrador.bia.services.sistema;

import com.iftm.integrador.bia.model.Sistema;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SistemaRepository extends JpaRepository<Sistema, Long> {

}
