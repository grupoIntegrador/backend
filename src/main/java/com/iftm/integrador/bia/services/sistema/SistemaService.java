package com.iftm.integrador.bia.services.sistema;

import com.iftm.integrador.bia.model.Sistema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SistemaService {
    @Autowired
    private SistemaRepository repository;

    public List<Sistema> getAll(){
        return repository.findAll();
    }

    public Sistema save(Sistema obj) {
        return repository.save(obj);
    }
}
