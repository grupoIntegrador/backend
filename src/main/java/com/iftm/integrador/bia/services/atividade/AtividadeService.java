package com.iftm.integrador.bia.services.atividade;

import com.iftm.integrador.bia.model.Atividade;
import com.iftm.integrador.bia.model.DTO.AtividadeResumedDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AtividadeService {
    @Autowired
    private AtividadeRepository repository;

    public List<Atividade> getAll() {
        return repository.findAll();
    }

    public Atividade save(Atividade obj) {
        return repository.save(obj);
    }

    public List<AtividadeResumedDTO> findAllResumed() {
        return repository.findAllResumed();
    }

    public List<AtividadeResumedDTO> findAllEntregues() {
        return repository.findAllEntregues();
    }

    public Optional<Atividade> findById(Long id) {
        return repository.findById(id);
    }

    @Modifying
    @Transactional
    public void enviarProducao(Long id) {
        repository.enviarProducao(id);
    }

}
