package com.iftm.integrador.bia.services.modulo;

import com.iftm.integrador.bia.model.Modulo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuloRepository extends JpaRepository<Modulo, Long> {

}
