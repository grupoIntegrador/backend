package com.iftm.integrador.bia.services.ambiente;

import com.iftm.integrador.bia.model.Ambiente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AmbienteService {
    @Autowired
    private AmbienteRepository repository;

    public List<Ambiente> getAll() {
        return repository.findAll();
    }

    public Ambiente save(Ambiente obj) {
        return repository.save(obj);
    }

    public Optional<List<Ambiente>> findAmbientesEntreguesByAtividade(Long id){
        return repository.findAmbientesEntreguesByAtividade(id);
    }


}
