package com.iftm.integrador.bia.exception;

public class NotFoundException extends Exception {

    public NotFoundException(Long id){
        super("Recurso não encontrado: " + id);
    }
}
